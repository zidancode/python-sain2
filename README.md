# Project Sain2 - Tugas Kuliah

```
   _ (`-. _  .-')                        ('-.           .-') _           .-')    ('-.                .-') _
  ( (OO  ( \( -O )                     _(  OO)         (  OO) )         ( OO ). ( OO ).-.           ( OO ) )
 _.`     \,------. .-'),-----.     ,--(,------.  .-----/     '._       (_)---\_)/ . --. / ,-.-'),--./ ,--,' .-----.
(__...--''|   /`. ( OO'  .-.  '.-')| ,||  .---' '  .--.|'--...__)      /    _ | | \-.  \  |  |OO|   \ |  |\/ ,-.   \
 |  /  | ||  /  | /   |  | |  ( OO |(_||  |     |  |('-'--.  .--'      \  :` `.-'-'  |  | |  |  |    \|  | '-'  |  |
 |  |_.' ||  |_.' \_) |  |\|  | `-'|  (|  '--. /_) |OO  ) |  |          '..`''.\| |_.'  | |  |(_|  .     |/   .'  /
 |  .___.'|  .  '.' \ |  | |  ,--. |  ||  .--' ||  |`-'|  |  |         .-._)   \|  .-.  |,|  |_.|  |\    |  .'  /__
 |  |     |  |\  \   `'  '-'  |  '-'  /|  `---(_'  '--'\  |  |         \       /|  | |  (_|  |  |  | \   | |       |
 `--'     `--' '--'    `-----' `-----' `------'  `-----'  `--'          `-----' `--' `--' `--'  `--'  `--' `-------'

                                             by Zidan Herlangga
```

## Deskripsi

Python Sains adalah proyek untuk tugas kuliah yang bertujuan untuk mengimplementasikan konsep-konsep ilmu sains menggunakan bahasa pemrograman Python.

## Menu / Fitur

- Konversi Suhu
- Konversi Bilangan Angka
- Temp/Suhu API

## Instalasi

1. Pastikan Python sudah terinstal di sistem Anda.

2. Clone repository ini ke direktori lokal Anda, dengan salin <br>
   `git clone https://gitlab.com/zidancode/python-sain2.git`.

3. Instal dependensi dengan menjalankan perintah <br>
   `pip install -r requipments.txt`.

## Penggunaan

1. Jalankan script utama dengan perintah:<br>
   `python main.py running`.

2. Ikuti petunjuk yang diberikan dalam aplikasi untuk menggunakan menu yang tersedia.

## Penulis

Zidan Herlangga

Jika ada pertanyaan atau masalah, jangan ragu untuk menghubungi saya di

email: zidanherlangga24@gmail.com
