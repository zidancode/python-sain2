import requests
import os

def Suhu():
    while True:
        try:
            os.system("cls" if os.name == "nt" else "clear")
            print("== SUHU ==")
            API_key = "API key"

            country = input("Masukan negara (id): ")
            kota = input("Masukan kota: ")

            data_cuaca = requests.get(f"https://api.openweathermap.org/data/2.5/weather?q={kota}&units=imperal&APPID={API_key}&lang={country}")

            if data_cuaca.json()["cod"] == "404":
                print("Kota tidak ditemukan!")
            else:
                cuaca = data_cuaca.json()["weather"][0]["main"]
                desc = data_cuaca.json()["weather"][0]["description"]
                temp = round(data_cuaca.json()["main"]["temp"])

                print("\nHasil")
                print(f"Cuaca di {kota} saat ini: {cuaca}")
                print(f"Deskripsi cuaca di {kota} saat ini: {desc}")
                print(f"Temperatur saat ini: {temp}°F")

            continue_ = input("\nLanjutkan? (y/n): ").lower().startswith("y")
            if not continue_:
                os.system("cls" if os.name == "nt" else "clear")
                break

        except KeyboardInterrupt:
            print("Program dibatalkan!")
            exit()
        except:
            print("Tidak boleh kosong!")
            continue

if __name__ == "__main__":
    Suhu()