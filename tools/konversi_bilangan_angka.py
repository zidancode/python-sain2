import os

def KonversiBilanganAngka():
    while True:
        try:
            os.system("cls" if os.name == "nt" else "clear")
            print("== KONVERSI BILANGAN ANGKA ==")
            
            dec = int(input("Masukan nilai Decimal: "))
            print("\nHasil")
            print("Decimal:     ", dec)
            print("Binary:      ", bin(dec).lstrip("0b"))
            print("Octal:       ", oct(dec).lstrip("0o"))
            print("Hexadecimal: ", hex(dec).lstrip("0x").upper())

            continue_ = input("\nLanjutkan? (y/n): ").lower().startswith("y")
            if not continue_:
                os.system("cls" if os.name == "nt" else "clear")
                break

        except KeyboardInterrupt:
            print("Program dibatalkan!")
            break
        except:
            print("Tidak boleh kosong!")
            continue

if __name__ == "__main__":
    KonversiBilanganAngka()