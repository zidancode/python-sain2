import os

def KonversiSuhu():
    while True:
        try:
            os.system("cls" if os.name == "nt" else "clear")
            print("== KONVERSI SUHU ==")
            
            cel = float(input("Masukan suhu Celcius: "))
            print("\nHasil")
            print("Celcius:     ", cel)
            print("Fahrenheit:  ", cel * 1.8 + 32)
            print("Kelvin:      ", cel + 273,15)
            print("Reamur:      ", cel * (4/5))
            print("Rankine:     ", (cel + 273.15) * 9/5)

            continue_ = input("\nLanjutkan? (y/n): ").lower().startswith("y")
            if not continue_:
                os.system("cls" if os.name == "nt" else "clear")
                break

        except KeyboardInterrupt:
            print("Program dibatalkan!")
            exit()
        except:
            print("Masukan nilai!")
            continue

if __name__ == "__main__":
    KonversiSuhu()