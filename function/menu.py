from tools.konversi_suhu import KonversiSuhu
from tools.konversi_bilangan_angka import KonversiBilanganAngka
from tools.suhu import Suhu

def menu():
    while True:
        try:
            print("==========================================================")
            print("MENU:")
            print("A. Konversi Suhu")
            print("B. Konversi Bilangan Angka")
            print("C. Suhu Hari Ini")
            
            print("\nMasih dalam pengembangan :)")
            
            print("\n0. Keluar")
            print("==========================================================")
            
            pilihan = input("Pilih menu yang tersedia diatas: ").lower()
            if pilihan == str(0):
                print("Keluar dari program...")
                break
            elif pilihan == "a":
                print("A. Konversi Suhu")
                KonversiSuhu()
            elif pilihan == "b":
                print("B. Konversi Bilangan Angka")
                KonversiBilanganAngka()
            elif pilihan == "c":
                print("C. Suhu Hari Ini")
                Suhu()
            else:
                print("Pilihan yang anda masukan tidak tersedia")
        
        except KeyboardInterrupt:
            print("Program dihentikan!")
            break
        except:
            print("Pilih sesuai menu diatas!")
            continue

if __name__ == "__main__":
    menu()