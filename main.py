import time
import os
import typer

# Bagian function
from function.banner import banner
from function.menu import menu

app = typer.Typer()

@app.command("running", help="Menjalankan program")
def main():
    # Bagian function
    banner()
    time.sleep(3)
    clear_screen()
    menu()

@app.command(help="Tampilkan versi program")
def version():
    typer.secho("Versi 2.0", fg=typer.colors.GREEN)
    exit()

@app.command(help="Periksa pembaruan terbaru")
def update():
    # Simulasi pembaruan sederhana
    time.sleep(2)
    typer.echo("Saat ini Anda di versi 2.0")
    exit()

@app.command("example", help="Contoh penggunaan: main.py [OPTIONS]")
def example():
    typer.echo("Contoh penggunaan: main.py [OPTIONS]")
    exit()

def clear_screen():
    """Membersihkan layar konsol"""
    os.system("cls" if os.name == "nt" else "clear")

if __name__ == "__main__":
    app()
